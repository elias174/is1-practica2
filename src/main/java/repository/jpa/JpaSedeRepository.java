package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.SedeRepository;
import domain.Sede;

@Repository
public class JpaSedeRepository extends JpaBaseRepository<Sede, Long> implements
		SedeRepository {
	@Override
	public Sede findByAddress(String address) {
		String jpaQuery = "SELECT a FROM Sede a WHERE a.address = :address";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("number",address);
		return getFirstResult(query);
	}

	@Override
	public Collection<Sede> findByBankId(Long bank_id) {
		String jpaQuery = "SELECT a FROM Account a JOIN a.owner_banks p WHERE p.id = :bankId";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("personId", bank_id);
		return query.getResultList();
	}
}