package repository;

import java.util.Collection;

import domain.Sede;

public interface SedeRepository extends BaseRepository<Sede, Long> {
	
	Sede findByAddress(String address);
	Collection<Sede> findByBankId(Long bank_id);

}