package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_sedes", indexes = { @Index(columnList = "number") })
public class Sede implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "sede_code_generator", sequenceName = "sede_code_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sede_code_generator")
	private Long code;

	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String address;

	public Sede() {
	}

	public Sede(Long code, String address) {
		this.code = code;
		this.address = address;
	}

	@Override
	public Long getId() {
		return this.code;
	}

	@Override
	public void setId(Long id) {
		this.code = id;
	}

	public String getAddress() {
		return this.address;
	}
	
	public void setAddress(String address){
		this.address = address;
	}

	public static Sede copy(Sede sede) {
		if (sede == null) {
			return null;
		}
		Sede copy = new Sede();
		copy.setAddress(sede.getAddress());
		return copy;
	}

	@Override
	public String toString() {
		return "{code: " + code + ", address: " + address + "}";
	}

	@ManyToMany(mappedBy = "banks")
	private Collection<Bank> owner_banks;

	
}
